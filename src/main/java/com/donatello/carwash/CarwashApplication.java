package com.donatello.carwash;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
@ComponentScan({"com.donatello.carwash.services","com.donatello.carwash"})
public class CarwashApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarwashApplication.class, args);
	}

}
